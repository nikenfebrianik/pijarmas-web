<meta charset="utf-8">
<title><?php echo $title ?></title>

<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="icon"
      type="image/png"
      href="<?php echo base_url(); ?>assets/img/favicon.png">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.css"  media="screen,projection"/>

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style media="screen">
body {
 display: flex;
 min-height: 100vh;
 flex-direction: column;
}

main {
 flex: 1 0 auto;
}
</style>
