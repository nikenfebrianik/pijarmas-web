<header class="navbar-fixed">
  <nav class="orange">
    <div class="nav-wrapper">
      <a href="<?php echo base_url(); ?>" class="brand-logo">
        <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width:100px;height:auto;">
      </a>
      <ul class="right hide-on-med-and-down">
        <li><a href="<?php echo base_url(); ?>about">Tentang Kami</a></li>
        <?php if ($this->session->userdata('logged_in')) {?>
          <!-- Dropdown Trigger -->
          <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Halo, <?php echo $user->fullname; ?>!<i class="material-icons right">arrow_drop_down</i></a></li>
        <?php } else {?>
          <li><a href="<?php echo base_url(); ?>login">Masuk</a></li>
          <li><a href="<?php echo base_url(); ?>register">Daftar</a></li>
        <?php }?>
      </ul>
    </div>
  </nav>

  <ul id="dropdown1" class="dropdown-content">
    <li><a href="#profile" class="modal-trigger orange-text"><i class="material-icons left">person</i>Profil</a></li>
    <?php if ($user->role_id == 'sel') {?>
    <li><a href="<?php echo base_url(); ?>my-store" class="modal-trigger orange-text"><i class="material-icons left">store</i>Kelola Toko</a></li>
    <?php } ?>
    <li><a href="<?php echo base_url(); ?>logout" class="orange-text"><i class="material-icons left">exit_to_app</i>Logout</a></li>
  </ul>
</header>

<?php if ($this->session->userdata('logged_in')) {?>
  <!-- Modal Structure -->
  <div id="profile" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Profil Saya</h4>
      <img class="materialboxed" style="margin: 0 auto;width:150px;height:auto;" src="uploads/profile/<?php echo $user->profile_path; ?>">
      <table>
        <tr>
          <td width="30%">Nama Lengkap</td>
          <td>:</td>
          <td width="70%"><?php echo $user->fullname; ?></td>
        </tr>
        <tr>
          <td>Email</td>
          <td>:</td>
          <td><?php echo $user->email; ?></td>
        </tr>
      </table>
      <?php if ($user->role_id == 'usr'): ?>
        Ingin mulai berjualan? <a href="<?php echo base_url(); ?>seller-start">Klik disini!</a>
      <?php endif; ?>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-light btn-flat orange-text">Tutup</a>
      <a href="<?php echo base_url(); ?>edit-profile" class="waves-effect waves-light orange btn">Edit Profil</a>
    </div>
  </div>
<?php }?>
