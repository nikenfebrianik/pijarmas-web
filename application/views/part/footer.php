<footer class="page-footer orange">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Kontak Kami</h5>
        <p class="grey-text text-lighten-4"><i class="material-icons left">contact_phone</i>+62 8122891962</p>
        <p class="grey-text text-lighten-4"><i class="material-icons left">email</i>p2hpdkpdemak@gmail.com</p>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    © 2019 Pijarmas Demak
    </div>
  </div>
</footer>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $(".dropdown-trigger").dropdown();
  $('.modal').modal();
  $('.tooltipped').tooltip();
  $('.materialboxed').materialbox();
});
</script>
