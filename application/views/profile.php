<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <?php
    $data['title'] = "Kelola Toko";
    $this->load->view("part/head", $data);
    ?>
</head>
<body>
<?php
$this->load->view("part/header")
?>
<main>
    <div class="row">
        <!-- Profile Section -->
        <div class="col s12 m3">
            <div class="card">
                <div class="card-image">
                    <img src="uploads/store/<?php echo $store->store_path; ?>" height="300"/>
                    <a class="tooltipped modal-trigger btn-floating halfway-fab waves-effect waves-light red"
                       data-position="bottom" data-tooltip="Ubah Foto Toko" href="#change-photo">
                        <i class="material-icons">photo_camera</i>
                    </a>

                    <!-- Modal Change Photo -->
                    <div id="change-photo" class="modal">
                        <div class="modal-content">
                            <div class="row">
                                <?php echo form_open_multipart("my-store/update/photo", "class='col s12'"); ?>
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>File</span>
                                        <input type="file" name="store_path">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Foto Toko" required
                                               aria-required="true">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a class="modal-close waves-effect waves-green btn-flat teal-text">Tutup</a>
                                    <button type="submit" class="waves-effect waves-green btn">Simpan</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <span class="card-title"><?php echo $store->name ?></span>
                    <p>
                        Alamat : <b><?php echo $store->address ?></b> <br>
                        No. Telepon : <b><?php echo $store->phone ?></b>
                    </p>
                </div>
                <div class="card-action">
                    <a href="#edit-store" class="modal-trigger">Edit Toko</a>

                    <!-- Modal Edit Toko -->
                    <div id="edit-store" class="modal">
                        <div class="modal-content">
                            <div class="row">
                                <?php echo form_open("my-store/update", "class='col s12'") ?>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="Nama Toko" id="storename" name="store" type="text"
                                               required aria-required="true"
                                               value="<?php echo $store->name ?>" class="validate">
                                        <label for="storename">Nama Toko</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s2">
                                        <input disabled value="+62" id="disabled" type="text" class="validate">
                                        <label for="disabled">No. Telepon</label>
                                    </div>
                                    <div class="input-field col s10">
                                        <input placeholder="contoh: 81234321123" name="phone" type="text"
                                               required aria-required="true"
                                               value="<?php echo $store->phone ?>" class="validate">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                          <textarea id="address" class="materialize-textarea" name="address" required
                                                    aria-required="true"
                                                    placeholder="contoh: Jalan Merdeka No. 1, Bandung, Jawa Barat"><?php echo $store->address ?></textarea>
                                        <label for="address">Alamat Lengkap</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a class="modal-close waves-effect waves-light btn-flat orange-text">Tutup</a>
                                    <button type="submit" class="waves-effect waves-light btn orange">Simpan</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Etalase Section-->
        <div class="col s12 m9">
            <h4>Etalase <a class="waves-effect waves-light orange btn modal-trigger" href="#add-product"><i
                            class="material-icons left">add</i>Tambah Produk</a></h4>
            <!-- Modal Add Product -->
            <div id="add-product" class="modal modal-fixed-footer">
                <?php echo form_open_multipart("product/add") ?>
                <div class="modal-content">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Nama Produk" id="product" name="product" type="text"
                                   required aria-required="true" class="validate">
                            <label for="product">Nama Produk</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                          <textarea id="description" class="materialize-textarea" name="description" required
                                    aria-required="true"
                                    placeholder="Deskripsi Produk"></textarea>
                            <label for="description">Deskripsi Produk</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Jumlah Produk" id="amount" name="amount" type="number"
                                   required aria-required="true" class="validate">
                            <label for="amount">Jumlah Produk</label>
                        </div>
                        <div class="input-field col s6">
                            <input placeholder="Harga Produk" id="price" name="price" type="number"
                                   required aria-required="true" class="validate">
                            <label for="price">Harga Produk</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn">
                                <span>File</span>
                                <input type="file" name="product_path">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Foto Produk" required
                                       aria-required="true">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="modal-close waves-effect waves-green btn-flat teal-text">Batal</a>
                    <button type="submit" class="waves-effect waves-green btn">Tambah</button>
                </div>
                </form>
            </div>
            <ul class="collection">
                <?php foreach ($etalase as $row) { ?>
                    <li class="collection-item avatar">
                        <img src="uploads/product/<?php echo $row->product_path; ?>" alt=""
                             class="circle"
                             style="left: 10px; width: 50px; height: 50px;">
                        <span class="title"><?php echo $row->name ?></span>
                        <p><?php echo $row->description ?></p>
                        <a href="<?php echo base_url(); ?>product/<?php echo $row->product_id; ?>"
                           class="secondary-content btn-flat waves-effect teal-text">Lihat Detail</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</main>
<?php
$this->load->view("part/footer")
?>
<?php if (isset($editStore)) {
    if ($editStore) {
        echo "<script>M.toast({html: 'Berhasil ubah Toko!'})</script>";
    } else {
        echo "<script>M.toast({html: 'Gagal ubah Toko!'})</script>";
    }
} ?>
<?php if (isset($upload)) {
    if ($upload) {
        echo "<script>M.toast({html: 'Berhasil ubah foto Toko!'})</script>";
    } else {
        echo "<script>M.toast({html: 'Gagal ubah foto Toko!'})</script>";
    }
} ?>
<?php if (isset($product)) {
    if ($product) {
        echo "<script>M.toast({html: 'Berhasil Tambah Produk!'})</script>";
    } else {
        echo "<script>M.toast({html: 'Gagal Tambah Produk!'})</script>";
    }
} ?>
</body>
</html>
