<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php
    $data['title'] = "Produk - ".$product->name;;
    $this->load->view("part/head", $data);
    ?>
  </head>
  <body>
    <?php
    $this->load->view("part/header")
    ?>
    <main class="container">
      <div class="row">
        <div class="card" style="width:500px;margin: 0 auto;margin-top:20px;">
          <div class="card-image waves-effect waves-block waves-light">
            <img style="height:auto;" class="activator" src="<?php echo base_url();?>uploads/product/<?php echo $product->product_path; ?>">
          </div>
          <div class="card-content">
            <span class="card-title grey-text text-darken-4"><?php echo $product->name; ?><a class="waves-effect activator waves-light btn right">Beli Produk</a></span>
            <table>
              <tr>
                <td width="10%">Deskripsi</td>
                <td>:</td>
                <td width="90%"><?php echo nl2br($product->description); ?></td>
              </tr>
              <tr>
                <td>Jumlah</td>
                <td>:</td>
                <td><?php echo $product->amount; ?></td>
              </tr>
              <tr>
                <td>Harga</td>
                <td>:</td>
                <td><?php echo $product->price; ?></td>
              </tr>
            </table>
          </div>
          <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">Detail Penjual <i class="material-icons right">close</i></span>
            <img height="100" width="100" class="circle" src="<?php echo base_url();?>uploads/store/<?php echo $store->store_path; ?>">
            <table>
              <tr>
                <td width="40%">Nama Toko/Penjual</td>
                <td>:</td>
                <td width="60%"><?php echo $store->name; ?></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>:</td>
                <td><?php echo $store->address; ?></td>
              </tr>
              <tr>
                <td>No. Telepon</td>
                <td>:</td>
                <td>0<?php echo $store->phone; ?></td>
              </tr>
            </table>
            <br>
            <?php if ($this->session->userdata('logged_in')): ?>
              <a href="https://wa.me/62<?php echo $store->phone; ?>" target="_blank" class="waves-effect waves-light btn"><i class="material-icons left">phone</i>Hubungi Penjual via WhatsApp</a>
            <?php else: ?>
              Silahkan login untuk dapat menghubungi Penjual. Klik <a href="<?php echo base_url(); ?>login">disini</a>!
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php if (isset($isOwner) && $isOwner) {?>
        <div class="row">
          <div class="col s6 m6">
            <a class="waves-effect waves-light btn-small modal-trigger right" href="#add-photo"><i class="material-icons left">photo</i>Tambah Foto</a>
          </div>
          <div class="col s6 m6">
            <a class="waves-effect waves-light btn-small modal-trigger left" href="#edit-product"><i class="material-icons left">edit</i>Edit Produk</a>
          </div>
        </div>
      <?php } else { ?>
        <h4 class="center">
          Galeri Produk
        </h4>
        <?php if (empty($photos)): ?>
          <p class="center">Penjual belum menambahkan foto galeri</p>
        <?php endif; ?>
      <?php }?>
      <div class="row">
        <?php foreach ($photos as $row) {?>
          <div class="col s6 m3">
            <img class="materialboxed" width="200" src="uploads/product/<?php echo $row->photo_path; ?>">
            <?php if ($this->session->userdata('logged_in')): ?>
              <br>
              <a href="#change-photo" class="modal-trigger">Ganti Foto</a>
              <!-- Modal Change Photo -->
              <div id="change-photo" class="modal modal-fixed-footer">
                <?php echo form_open_multipart("product/change-gallery") ?>
                <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                 <input type="hidden" name="existing_path" value="<?php echo $row->photo_path; ?>">
                  <div class="modal-content">
                      <div class="row">
                          <div class="file-field input-field col s12">
                              <div class="btn">
                                  <span>File</span>
                                  <input type="file" name="photo">
                              </div>
                              <div class="file-path-wrapper">
                                  <input class="file-path validate" type="text" placeholder="Foto Produk">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <a class="modal-close waves-effect waves-green btn-flat teal-text">Batal</a>
                      <button type="submit" class="waves-effect waves-green btn">Ganti</button>
                  </div>
                </form>
              </div>
            <?php endif; ?>
          </div>
        <?php } ?>
     </div>
     <!-- Modal Edit Product -->
     <div id="edit-product" class="modal modal-fixed-footer">
       <?php echo form_open_multipart("product/update") ?>
        <input type="hidden" name="id" value="<?php echo $product->product_id; ?>">
         <div class="modal-content">
             <div class="row">
                 <div class="input-field col s12">
                     <input placeholder="Nama Produk" id="product" name="product" type="text"
                            required aria-required="true" value="<?php echo $product->name; ?>" class="validate">
                     <label for="product">Nama Produk</label>
                 </div>
             </div>
             <div class="row">
                 <div class="input-field col s12">
                     <textarea id="description" class="materialize-textarea" name="description" required
                               aria-required="true"
                               placeholder="Deskripsi Produk"><?php echo $product->description; ?></textarea>
                     <label for="description">Deskripsi Produk</label>
                 </div>
             </div>
             <div class="row">
                 <div class="input-field col s6">
                     <input placeholder="Jumlah Produk" id="amount" name="amount" type="number"
                            required aria-required="true" class="validate" value="<?php echo $product->amount; ?>">
                     <label for="amount">Jumlah Produk</label>
                 </div>
                 <div class="input-field col s6">
                     <input placeholder="Harga Produk" id="price" name="price" type="number"
                            required aria-required="true" class="validate" value="<?php echo $product->price; ?>">
                     <label for="price">Harga Produk</label>
                 </div>
             </div>
             <div class="row">
                 <div class="file-field input-field col s12">
                     <div class="btn">
                         <span>File</span>
                         <input type="file" name="product_path">
                     </div>
                     <div class="file-path-wrapper">
                         <input class="file-path validate" type="text" placeholder="Foto Produk">
                     </div>
                 </div>
             </div>
         </div>
         <div class="modal-footer">
             <a class="modal-close waves-effect waves-green btn-flat teal-text">Batal</a>
             <button type="submit" class="waves-effect waves-green btn">Tambah</button>
         </div>
       </form>
     </div>
      <!-- Modal Add Photo -->
      <div id="add-photo" class="modal">
        <div class="modal-content">
          <div class="row">
            <?php echo form_open_multipart("product/add-gallery", "class='col s12'"); ?>
            <input type="hidden" name="id" value="<?php echo $product->product_id ?>">
              <div class="file-field input-field">
                <div class="btn">
                  <span>File</span>
                  <input type="file" name="photo_paths[]" multiple>
                </div>
                <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Foto Produk" required
                      aria-required="true">
                </div>
              </div>
              <div class="modal-footer">
                <a class="modal-close waves-effect waves-green btn-flat teal-text">Tutup</a>
                <button type="submit" class="waves-effect waves-green btn">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </main>
    <?php
    $this->load->view("part/footer")
    ?>
    <?php if (isset($edit)) {
        if ($edit) {
          echo "<script>M.toast({html: 'Berhasil ubah Produk!'})</script>";
        } else {
          echo "<script>M.toast({html: 'Gagal ubah Produk!'})</script>";
        }
    } ?>
  </body>
</html>
