<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php
    $data['title'] = "Beranda";
    $this->load->view("part/head", $data);
    ?>
  </head>
  <body>
    <?php
    $this->load->view("part/header")
    ?>
    <main>
      <div class="row">
        <?php foreach ($allProduct as $row) {
          $this->load->model('store');
          $query = $this->store->getStore($row->store_id); ?>
          <div class="col s12 m3">
            <div class="card medium">
              <div class="card-image">
                <img src="uploads/product/<?php echo $row->product_path; ?>">
              </div>
              <div class="card-content">
                <span class="card-title"><?php echo $row->name; ?></span>
                <span class="card-desc">oleh <?php echo $query->row()->name; ?></span> <br>
              </div>
              <div class="card-action">
                <a href="<?php echo base_url(); ?>product/<?php echo $row->product_id; ?>" class="orange-text">Lihat Detail</a>
              </div>`
            </div>
          </div>
        <?php } ?>
      </div>
    </main>
    <?php
    $this->load->view("part/footer")
    ?>
    <?php if (isset($success)): ?>
      <?php if ($success): ?>
        <script>M.toast({html: 'Anda berhasil login!'})</script>
      <?php endif; ?>
    <?php endif; ?>
    <?php if (isset($edit)): ?>
      <?php if ($edit): ?>
        <script>M.toast({html: 'Edit Profil berhasil!'})</script>
      <?php endif; ?>
    <?php endif; ?>
    <?php if (isset($start)): ?>
      <?php if ($start): ?>
        <script>M.toast({html: 'Pendaftaran Toko Berhasil!'})</script>
      <?php endif; ?>
    <?php endif; ?>
    <?php if (isset($logout)): ?>
      <?php if ($logout): ?>
        <script>M.toast({html: 'Anda berhasil logout!'})</script>
      <?php endif; ?>
    <?php endif; ?>
  </body>
</html>
