<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php
    $data['title'] = "Tentang Pijarmas";
    $this->load->view("part/head", $data);
    ?>
  </head>
  <body>
    <?php
    $this->load->view("part/header")
    ?>
    <main class="container">
      <div class="row">
        <h2>Tentang Pijarmas</h2>
        <div class="col s12 m12">
          <div class="card-panel orange">
            <span class="white-text">
              PIJARMAS, PUSAT INFORMASI DAN JARINGAN PEMASARAN PRODUK OLAHAN PERIKANAN DI KABUPATEN DEMAK. SEMOGA DENGAN ADANYA PROYEK PERUBAHAN INI, MAMPU MEMPERLUAS JANGKUAN PEMASARAN DAN MENINGKATKAN OMSET PENJUALAN, SEHINGGA DAPAT MENINGKATKAN PENDAPATAN DAN KESEJAHTERAAN PARA PENGOLAH DAN PEMASAR PRODUK OLAHAN HASIL PERIKANAN DI KABUPATEN DEMAK.
            </span>
          </div>
        </div>
      </div>
    </main>
    <?php
    $this->load->view("part/footer")
    ?>
  </body>
</html>
