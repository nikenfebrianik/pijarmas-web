<footer class="footer">
    <div class="container-fluid">
        <div class="copyright float-right small">
        &copy; Pijarmas Demak 2019, 
        desain tema oleh <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
        </div>
    </div>
</footer>