<!DOCTYPE html>
<html lang="en">
<?php
$data['title'] = "Daftar Penjual";
$this->load->view("admin/head", $data)
?>
<body class="">
  <div class="wrapper ">
    <?php
    $data['page'] = "store";
    $this->load->view("admin/sidebar", $data) 
    ?>
    <div class="main-panel">
      <?php
      $data['page'] = "Daftar Penjual";
      $this->load->view("admin/navbar", $data) 
      ?>
      <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-warning">
                            <h4 class="card-title">Daftar Penjual</h4>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-hover">
                                <thead class="text-warning">
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>No. Telepon</th>
                                </thead>
                                <tbody>
                                <?php foreach($stores->result() as $row) { ?>
                                <tr>
                                    <td><?php echo $row->name ?></td>
                                    <td><?php echo $row->address ?></td>
                                    <td><?php echo $row->phone == "" ? "":"0".$row->phone ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
      </div>
      <?php
      $this->load->view('admin/footer'); 
      ?>
    </div>
  </div>
  <?php
  $this->load->view("admin/script")
  ?>
</body>
</html>