<div class="sidebar" data-color="orange" data-background-color="white" data-image="<?php echo base_url(); ?>assets/admin/img/sidebar-3.jpg">
    <div class="logo">
        <a href="<?php echo base_url(); ?>admin/home" class="simple-text logo-normal">
            Pijarmas Admin
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item <?php echo $page == 'home'? 'active':'' ?>">
                <a class="nav-link" href="<?php echo base_url(); ?>admin/home">
                    <i class="material-icons">dashboard</i>
                    <p>Beranda</p>
                </a>
            </li>
            <li class="nav-item <?php echo $page == 'user'? 'active':'' ?>">
                <a class="nav-link" href="<?php echo base_url(); ?>admin/users">
                    <i class="material-icons">people</i>
                    <p>Daftar Pengguna</p>
                </a>
            </li>
            <li class="nav-item <?php echo $page == 'store'? 'active':'' ?>">
                <a class="nav-link" href="<?php echo base_url(); ?>admin/stores">
                    <i class="material-icons">store</i>
                    <p>Daftar Penjual</p>
                </a>
            </li>
        </ul>
    </div>
</div>
