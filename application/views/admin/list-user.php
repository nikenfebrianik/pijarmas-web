<!DOCTYPE html>
<html lang="en">
<?php
$data['title'] = "Daftar Pengguna";
$this->load->view("admin/head", $data)
?>
<body class="">
  <div class="wrapper ">
    <?php
    $data['page'] = "user";
    $this->load->view("admin/sidebar", $data) 
    ?>
    <div class="main-panel">
      <?php
      $data['page'] = "Daftar Pengguna";
      $this->load->view("admin/navbar", $data) 
      ?>
      <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-warning">
                            <h4 class="card-title">Daftar Pengguna</h4>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-hover">
                                <thead class="text-warning">
                                    <th>Nama</th>
                                    <th>Email</th>
                                </thead>
                                <tbody>
                                <?php foreach($users->result() as $row) { ?>
                                <tr>
                                    <td><?php echo $row->fullname ?></td>
                                    <td><?php echo $row->email ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
      </div>
      <?php
      $this->load->view('admin/footer'); 
      ?>
    </div>
  </div>
  <?php
  $this->load->view("admin/script")
  ?>
</body>
</html>