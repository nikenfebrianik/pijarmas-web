<!DOCTYPE html>
<html lang="en">
<?php
$data['title'] = "Beranda";
$this->load->view("admin/head", $data)
?>
<body class="">
  <div class="wrapper ">
    <?php
    $data['page'] = "home";
    $this->load->view("admin/sidebar", $data)
    ?>
    <div class="main-panel">
      <?php
      $data['page'] = "Beranda";
      $this->load->view("admin/navbar", $data)
      ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">people</i>
                  </div>
                  <p class="card-category">Pengguna Terdaftar</p>
                  <h3 class="card-title"><?php echo $countUser ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">people</i>
                    <a href="<?php echo base_url(); ?>users">Lihat Daftar Pengguna</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">store</i>
                  </div>
                  <p class="card-category">Penjual Terdaftar</p>
                  <h3 class="card-title"><?php echo $countStore ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">store</i>
                    <a href="<?php echo base_url(); ?>stores">Lihat Daftar Penjual</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
      $this->load->view('admin/footer');
      ?>
    </div>
  </div>
  <?php
  $this->load->view("admin/script")
  ?>
</body>
</html>
