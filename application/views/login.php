<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Pijarmas</title>
    <link rel="icon"
          type="image/png"
          href="<?php echo base_url(); ?>assets/img/favicon.png">

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" media="screen,projection"/>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<div class="section"></div>
<main>
    <div style="text-align: center;">
        <h5 class="orange-text">Masuk</h5>
        <?php if (isset($success) && !$success) {
          echo '<p class="red-text">Gagal masuk! Email/Kata Kunci Anda salah atau belum terdaftar.</p>';
        }?>
        <?php if (isset($register) && $register) {
          echo '<p class="green-text">Berhasil daftar akun! Silahkan masuk.</p>';
        }?>
        <?php if (isset($forgot) && $forgot == "success reset") {
          echo '<p class="green-text">Berhasil ubah kata sandi! Silahkan masuk menggunakan kata sandi baru.</p>';
        }?>
        <div class="section"></div>

        <div class="container">
            <div class="z-depth-1 grey lighten-4 row"
                 style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
                <?php echo form_open("validate", "class='col s12'") ?>
                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='email' name='email' id='email'/>
                        <label for='email'>Masukkan Email</label>
                    </div>
                </div>

                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='password' name='password' id='password'/>
                        <label for='password'>Masukkan Kata Sandi</label>
                    </div>
                    <label class="left" style="padding: 0 .75rem">
                        <input id="show" type="checkbox" class="filled-in" />
                        <span>Lihat Kata Sandi</span>
                    </label>
                </div>

                <div style="text-align: center;">
                    <div class='row'>
                        <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect orange'>Masuk
                        </button>
                    </div>
                    <div class="row">
                        <a href='forgot' class="orange-text">Lupa Kata Sandi</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
        Belum punya akun ? <a href="<?php echo base_url(); ?>register" class="orange-text">Daftar disini!</a>

    </div>

    <div class="section"></div>
    <div class="section"></div>
</main>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>
<script>
    var toggle = 0;//to keep track of toggling changes
    $(document).ready(function () {
        $('#show').click(function () {
            if (toggle == 0) {
                $('#password').prop("type", "text");
                toggle = 1;//will be 1 when changed to text
            } else {
                $('#password').prop('type', 'password');
                toggle = 0;//0 when changed to password
            }
        });
    });
</script>
<?php if (isset($login)): ?>
  <?php if (!$login): ?>
    <script>M.toast({html: 'Anda belum Login!'})</script>
  <?php endif; ?>
<?php endif; ?>
</body>
</html>
