<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <?php
    $data['title'] = "Buat Toko";
    $this->load->view("part/head", $data);
    ?>
</head>
<body>
<?php
$this->load->view("part/header")
?>
<main class="container">
    <div class="row">
      <h4>Pendaftaran Toko/Penjual</h4>
    </div>
    <div class="row">
      <?php echo form_open_multipart("seller-start/submit", "class='col s12'") ?>
        <div class="row">
          <div class="input-field col s12">
            <input placeholder="Masukkan Nama Toko Anda" id="store" type="text" name="store" class="validate">
            <label for="store">Nama Toko</label>
          </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input placeholder="contoh: 081234321123" id="phone" name="phone" type="text"
                       required aria-required="true"
                       value="" class="validate">
                <label for="phone">No. Telepon</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                    <textarea id="address" class="materialize-textarea" name="address" required
                              aria-required="true"
                              placeholder="contoh: Jalan Merdeka No. 1, Bandung, Jawa Barat"></textarea>
                <label for="address">Alamat Lengkap</label>
            </div>
        </div>
        <div class="row">
          <div class="file-field input-field col s12">
              <div class="btn">
                  <span>File</span>
                  <input type="file" name="store_path">
              </div>
              <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Foto Toko" required
                         aria-required="true">
                  <span>Gunakan file JPG/PNG berukuran maks. 5 MB</span>
              </div>
          </div>
        </div>
        <button type="submit" class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>Daftar</button>
      </form>
    </div>
</main>
<?php
$this->load->view("part/footer")
?>
<?php if (isset($start)): ?>
  <?php if (!$start): ?>
    <script>M.toast({html: 'Pendaftaran Toko Gagal!'})</script>
  <?php endif; ?>
<?php endif; ?>
</body>
</html>
