<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <?php
    $data['title'] = "Ubah Profil";
    $this->load->view("part/head", $data);
    ?>
</head>
<body>
<?php
$this->load->view("part/header")
?>
<main class="container">
    <div class="row">
      <h4>Edit Profil</h4>
    </div>
    <div class="row">
      <?php echo form_open_multipart("edit-profile/update", "class='col s12'") ?>
        <div class="row">
            <div class="input-field col s12">
                <input placeholder="Nama Lengkap" id="fullname" name="fullname" type="text"
                       required aria-required="true"
                       value="<?php echo $user->fullname; ?>" class="validate">
                <label for="fullname">Nama Lengkap</label>
            </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
              <input disabled placeholder="Email" id="email" type="email"
                     value="<?php echo $user->email;; ?>" class="validate">
              <label for="email">Email</label>
          </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>File</span>
                <input type="file" name="profile_path">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Foto Profil">
                <span>Gunakan file JPG/PNG berukuran maks. 10 MB</span>
            </div>
        </div>

        <button type="submit" class="waves-effect waves-green btn">Simpan</button>
      </form>
    </div>
</main>
<?php
$this->load->view("part/footer")
?>
<?php if (isset($edit)): ?>
  <?php if (!$edit): ?>
    <script>M.toast({html: 'Edit Profil Gagal!'})</script>
  <?php endif; ?>
<?php endif; ?>
</body>
</html>
