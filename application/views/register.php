<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Pijarmas</title>
    <link rel="icon"
          type="image/png"
          href="<?php echo base_url(); ?>assets/img/favicon.png">

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" media="screen,projection"/>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<div class="section"></div>
<main>
    <center>
        <h5 class="orange-text">Daftar Akun Baru</h5>
        <?php if (isset($register)) {
            if (!$register) {
                echo '<p class="red-text">Gagal Daftar! Email Anda sudah terdaftar.</p>';
            }
        }?>
        <div class="section"></div>
        <div class="container">
            <div class="z-depth-1 grey lighten-4 row" style="width: 50%;
				display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

                <?php echo form_open("add-user", "class='col s12'") ?>
                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='text' name='fullname' id='fullname'/>
                        <label for='fullname'>Nama Lengkap</label>
                    </div>
                </div>

                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='email' name='email' id='email'/>
                        <label for='email'>Email</label>
                    </div>
                </div>

                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='password' name='password' id='password'/>
                        <label for='password'>Kata Sandi</label>
                    </div>
                </div>

                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='password' name='confirm' id='confirm'/>
                        <label for='confirm'>Konfirmasi Kata Sandi</label>
                        <span id="message" class="left red-text">Kata Sandi tidak boleh kosong!</span>
                    </div>
                </div>

                <br/>
                <center>
                    <div class='row'>
                        <button type='submit' id="registerBtn" name='btn_login' class='col s12 btn btn-large waves-effect orange disabled'>
                            Daftar
                        </button>
                    </div>
                </center>
                </form>
            </div>
        </div>
        Sudah punya akun ? <a href="<?php echo base_url(); ?>login" class="orange-text">Silahkan Masuk</a>
    </center>

    <div class="section"></div>
    <div class="section"></div>
</main>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>
<script>
$('#password, #confirm').on('keyup', function () {
    if ($('#password').val() == $('#confirm').val() && $('#password').val() != "") {
      $('#message').removeClass('red-text');
      $('#message').html('Kata Sandi sesuai!');
      $('#message').addClass('green-text');
      $('#registerBtn').removeClass('disabled');
    } else if ($('#password').val() == "") {
      $('#message').removeClass('green-text');
      $('#message').html('Kata Sandi tidak boleh kosong!');
      $('#message').addClass('red-text');
      $('#registerBtn').addClass('disabled');
    } else {
      $('#message').removeClass('green-text');
      $('#message').html('Kata Sandi tidak sesuai!');
      $('#message').addClass('red-text');
      $('#registerBtn').addClass('disabled');
    }
});
</script>
</body>
</html>
