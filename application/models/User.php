<?php

class User extends CI_Model
{
    public function getProfile($email)
    {
      $data = array(
          'email' => $email
      );
        return $this->db->get_where('users', $data);
    }

    public function getAllProfile()
    {
        return $this->db->get('users');
    }

    public function countAllUsers() {
        return $this->db->count_all('users');
      }

    public function validate($email, $password)
    {
        return $this->db->get_where('users', array('email' => $email, 'password' => $password));
    }

    public function create($fullname, $email, $password)
    {
        $data = array(
            'fullname' => $fullname,
            'email' => $email,
            'password' => $password,
            'role_id' => 'usr'
        );

        $check = $this->db->get_where('users', array('email' => $email));
        if ($check->num_rows() > 0) {
            return 0;
        } else {
            $this->db->insert('users', $data);
            return $this->db->affected_rows();
        }
    }

    public function edit($email, $fullname, $filename)
    {
        if (empty($filename)){
            $profile = array(
                'fullname' => $fullname
            );
        } else {
            $profile = array(
                'fullname' => $fullname,
                'profile_path' => $filename
            );
        }

        $this->db->where('email', $email);
        $this->db->update('users', $profile);

        return $this->db->affected_rows();
    }

    public function changePassword($email, $password)
    {
      $data = array(
        'password' => $password
      );
      $this->db->where('email', $email);
      $this->db->update('users', $data);

      return $this->db->affected_rows();
    }

    public function assignStore($id, $email){
      $data = array(
          'store_id' => $id,
          'role_id' => 'sel'
      );

      $this->db->where('email', $email);
      $this->db->update('users', $data);

      return $this->db->affected_rows();
    }
}
