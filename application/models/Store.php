<?php

class Store extends CI_Model
{
  public $name, $address, $phone, $path;
    public function create(Store $store)
    {
      $data = array(
          'name' => $store->name,
          'address' => $store->address,
          'phone' => $store->phone
      );

      $this->db->insert('stores', $data);
      return $this->db->insert_id();
    }

    public function getAllStore()
    {
        return $this->db->get('stores');
    }

    public function countAllStores() {
      return $this->db->count_all('stores');
    }

    public function getStore($id)
    {
      $data = array(
          'store_id' => $id
      );

      return $this->db->get_where('stores', $data);
    }

    public function edit($storeId, Store $store)
    {
      $data = array(
          'name' => $store->name,
          'address' => $store->address,
          'phone' => $store->phone
      );
      $this->db->where('store_id', $storeId);
      $this->db->update('stores', $data);

      return $this->db->affected_rows();
    }

    public function editPhoto($storeId, $filename)
    {
        $data = array(
            'store_path' => $filename,
        );
        $this->db->where('store_id', $storeId);
        $this->db->update('stores', $data);

        return $this->db->affected_rows();
    }
}
