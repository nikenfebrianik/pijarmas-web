<?php
/**
 * Created by PhpStorm.
 * User: yudan
 * Date: 5/9/2019
 * Time: 6:33 AM
 */

class Product extends CI_Model
{
    public $name, $desc, $amount, $price, $path, $storeid;

    public function getEtalase($storeid) {
        $data = array(
            'store_id' => $storeid
        );
        return $this->db->get_where('products', $data);
    }

    public function getAll() {
      $this->db->select('*');
      $this->db->from('products');
      return $this->db->get();
    }

    public function getProduct($productId) {
      $data = array(
          'product_id' => $productId
      );
      return $this->db->get_where('products', $data);
    }

    public function getGallery($productId) {
      $data = array(
          'product_id' => $productId
      );
      return $this->db->get_where('product_galleries', $data);
    }

    public function create(Product $product)
    {
        $data = array(
            'name' => $product->name,
            'description' => $product->desc,
            'amount' => $product->amount,
            'price' => $product->price,
            'product_path' => $product->path,
            'store_id' => $product->storeId
        );

        $this->db->insert('products', $data);
        return $this->db->affected_rows();
    }

    public function edit($productId, Product $product)
    {
        if (empty($product->path)){
            $data = array(
                'name' => $product->name,
                'description' => $product->desc,
                'amount' => $product->amount,
                'price' => $product->price
            );
        } else {
            $data = array(
                'name' => $product->name,
                'description' => $product->desc,
                'amount' => $product->amount,
                'price' => $product->price,
                'product_path' => $product->path
            );
        }

        $this->db->where('product_id', $productId);
        $this->db->update('products', $data);

        return $this->db->affected_rows();
    }

    public function addGallery($productId, $filename) {

      $data = array(
        'photo_path' => $filename,
        'product_id' => $productId
      );

      $this->db->insert('product_galleries', $data);
      return $this->db->affected_rows();
    }
}
