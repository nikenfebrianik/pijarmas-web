<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller
{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$userEmail = $this->session->userdata('email');
		$user = $this->user->getProfile($userEmail);

		$data['logout'] = $this->session->flashdata('logout');
		$data['success'] = $this->session->flashdata('success');
		$data['edit'] = $this->session->flashdata('edit');
		$data['start'] = $this->session->flashdata('start');
		$data['user'] = $user->row();
		$data['allProduct'] = $this->product->getAll()->result();

    $this->load->view('home', $data);
	}

	public function about() {
		$userEmail = $this->session->userdata('email');
		$user = $this->user->getProfile($userEmail);
		$data['user'] = $user->row();

		$this->load->view('about', $data);
	}
}
