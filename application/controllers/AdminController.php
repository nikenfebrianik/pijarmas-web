<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$logged = $this->session->userdata('logged_in');
		if (isset($logged) != null) {
			if (!$this->session->userdata('is_admin')) {
				redirect('/login');
			}
		} else {
			redirect('/login');
		}
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['countStore'] = $this->store->countAllStores();
		$data['countUser'] = $this->user->countAllUsers();
		$this->load->view('admin/home', $data);
	}

	public function listUser()
	{
		$data['users'] = $this->user->getAllProfile();
		$this->load->view('admin/list-user', $data);
	}
	
	public function listStore()
	{
		$data['stores'] = $this->store->getAllStore();
		$this->load->view('admin/list-store', $data);
	}
}
