<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller
{
    public function __construct() {
      parent::__construct();
      // get user session
      $loginStatus= $this->session->userdata('logged_in');
      if (!$loginStatus) {
        $this->session->set_flashdata('login', false);
        redirect('login');
      }
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
      $userEmail = $this->session->userdata('email');
      $user = $this->user->getProfile($userEmail);
      $store = $this->store->getStore($user->row()->store_id);
      $etalase = $this->product->getEtalase($user->row()->store_id);

      $data['user'] = $user->row();
      $data['store'] = $store->row();
      $data['etalase'] = $etalase->result();
      $data['editStore'] = $this->session->flashdata('store');
      $data['upload'] = $this->session->flashdata('upload');
      $data['product'] = $this->session->flashdata('product');

      $this->load->view('profile', $data);
    }

    public function editProfile()
    {
      $userEmail = $this->session->userdata('email');
      $user = $this->user->getProfile($userEmail);

      $data['user'] = $user->row();
      $data['edit'] = $this->session->flashdata('edit');

      $this->load->view('edit-profile', $data);
    }

    public function changePhoto()
    {
      $userProfile = $this->user->getProfile($this->session->userdata('email'));

      $user = explode('@', $this->session->userdata('email'));
      $filename = $user[0] . '-store-photo';
      $config['upload_path'] = './uploads/store';
      $config['file_name'] = $filename;
      $config['overwrite'] = true;
      $config['allowed_types'] = 'jpg|png';
      $config['max_size'] = 10240;

      $this->load->library('upload', $config);
      $this->upload->do_upload('store_path');

      $path = $filename.$this->upload->data('file_ext');
      if (empty($_FILES['store_path']['name'])) {
        $path = "";
      }

      $result = $this->store->editPhoto($userProfile->row()->store_id, $path);

      if ($result > 0) {
        $this->session->set_flashdata('upload', true);
        redirect('my-store');
      } else {
        $this->session->set_flashdata('upload', false);
        redirect('my-store');
      }
    }

    public function updateStore() {
      $userEmail = $this->session->userdata('email');
      $user = $this->user->getProfile($userEmail);

      $name = $this->input->post('store');
      $address = $this->input->post('address');
      $phone = $this->input->post('phone');

      $editStore = new Store();
      $editStore->name = $name;
      $editStore->address = $address;
      $editStore->phone = $phone;

      $result = $this->store->edit($user->row()->store_id, $editStore);

      if ($result > 0) {
        // success
        $this->session->set_flashdata('store', true);
        redirect('my-store');
      } else {
        $this->session->set_flashdata('store', false);
        redirect('my-store');
      }
    }

    public function update()
    {
        $name = $this->input->post('fullname');

        $user = explode('@', $this->session->userdata('email'));
        $filename = $user[0] . '-profile-photo';
        $config['upload_path'] = './uploads/profile';
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 10240;

        $this->load->library('upload', $config);
        $this->upload->do_upload('profile_path');

        $path = $filename.$this->upload->data('file_ext');
        if (empty($_FILES['profile_path']['name'])) {
          $path = "";
        }
        $result = $this->user->edit($this->session->userdata('email'), $name, $path);
        if ($result > 0) {
          $this->session->set_flashdata('edit', true);
          redirect('/');
        } else {
          $this->session->set_flashdata('edit', false);
          redirect('edit-profile');
        }
    }

    public function sellerStart() {
      $userEmail = $this->session->userdata('email');
      $user = $this->user->getProfile($userEmail);

      $data['start'] = $this->session->flashdata('start');
      $data['user'] = $user->row();
      $this->load->view('start', $data);
    }

    public function registerStore() {
      $name = $this->input->post('store');
      $address = $this->input->post('address');
      $phone = $this->input->post('phone');

      $newStore = new Store();
      $newStore->name = $name;
      $newStore->address = $address;
      $newStore->phone = $phone;

      $user = explode('@', $this->session->userdata('email'));
      $filename = $user[0] . '-store-photo';
      $config['upload_path'] = './uploads/store';
      $config['file_name'] = $filename;
      $config['overwrite'] = true;
      $config['allowed_types'] = 'jpg|png';
      $config['max_size'] = 10240;

      $this->load->library('upload', $config);
      $this->upload->do_upload('store_path');

      $newStore->path = $filename.$this->upload->data('file_ext');
      if (empty($_FILES['store_path']['name'])) {
        $newStore->path = "";
      }

      $storeId = $this->store->create($newStore);
      $result = $this->user->assignStore($storeId, $this->session->userdata('email'));

      if ($result > 0) {
        // success
        $this->session->set_flashdata('start', true);
        redirect('/');
      } else {
        $this->session->set_flashdata('start', false);
        redirect('seller-start');
      }
    }
}
