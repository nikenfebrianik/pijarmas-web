<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    // Halaman Login
    public function index()
    {
      // get user session
   		$loginStatus = $this->session->userdata('logged_in');
   		$isAdmin = $this->session->userdata('is_admin');
  		if ($loginStatus && $isAdmin) {
  			redirect('/admin');
  		} else if ($loginStatus && !$isAdmin){
        redirect('/');
      }
      $data['login'] = $this->session->flashdata('login');
      $data['forgot'] = $this->session->flashdata('forgot');
      $data['success'] = $this->session->flashdata('success');
      $data['register'] = $this->session->flashdata('register');
      $this->load->view('login', $data);
    }

    // Login Process
    public function validateUser()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $query = $this->user->validate($email, $password);
        if ($query->num_rows()) {
            $user = array(
                'email' => $email,
                'logged_in' => TRUE,
                'is_admin' => FALSE
            );
            if ($query->result()->role == "adm") {
              $user['is_admin'] = TRUE;
            }
            $this->session->set_userdata($user);
            $this->session->set_flashdata('success', true);
            if ($user['is_admin']) {
              redirect('/admin');
            } else {
              redirect('/');
            }
        } else {
            $this->session->set_flashdata('success', false);
            redirect('/login');
        }
    }

    public function logout()
    {
      $this->session->unset_userdata('email');
      $this->session->unset_userdata('logged_in');
      $this->session->set_flashdata('logout', true);

      redirect('/');
    }

    public function register()
    {
      // get user session
   		$loginStatus= $this->session->userdata('logged_in');
  		if ($loginStatus) {
  			redirect('/');
  		}
      $data['register'] = $this->session->flashdata('register');
      $this->load->view('register', $data);
    }

    public function addUser()
    {
        $fullname = $this->input->post('fullname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $result = $this->user->create($fullname, $email, $password);
        if ($result > 0) {
            $this->session->set_flashdata('register', true);
            redirect('/');
        } else {
            $this->session->set_flashdata('register', false);
            redirect('register');
        }
    }

    public function forgot()
    {
      // get user session
      $loginStatus= $this->session->userdata('logged_in');
   		$data['forgot'] = $this->session->flashdata('forgot');
  		if ($loginStatus) {
  			redirect('/');
  		}
      $this->load->view('forgot', $data);
    }

    public function resetPassword()
    {
      $email = $this->input->post('email');
      $password = $this->input->post('password');

      $res = $this->user->getProfile($email);
      if ($res->num_rows()) {
        if ($this->user->changePassword($email, $password) > 0) {
          $this->session->set_flashdata('forgot', "success reset");
          redirect('login');
        } else {
          $this->session->set_flashdata('forgot', "failed reset");
          redirect('forgot');
        }
      }
      $this->session->set_flashdata('forgot', "wrong email");
      redirect('forgot');
    }
}
