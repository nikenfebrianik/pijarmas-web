<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function detail($id)
    {
        $userEmail = $this->session->userdata('email');
        $user = $this->user->getProfile($userEmail);

        $product = $this->product->getProduct($id);
        $gallery = $this->product->getGallery($id);
        $store = $this->store->getStore($product->row()->store_id);

        if ($this->session->userdata('logged_in')) {
            if ($user->row()->store_id == $product->row()->store_id) {
                $data['isOwner'] = true;
            }
        }
        $data['user'] = $user->row();
        $data['product'] = $product->row();
        $data['store'] = $store->row();
        $data['photos'] = $gallery->result();
        $data['edit'] = $this->session->flashdata('edit');

        $this->load->view('product', $data);
    }

    public function update()
    {
        $userProfile = $this->user->getProfile($this->session->userdata('email'));

        $productId = $this->input->post('id');
        $product = $this->input->post('product');
        $desc = $this->input->post('description');
        $amount = $this->input->post('amount');
        $price = $this->input->post('price');

        $editProduct = new Product();
        $editProduct->name = $product;
        $editProduct->desc = $desc;
        $editProduct->amount = $amount;
        $editProduct->price = $price;

        $user = explode('@', $userProfile->row()->email);
        $filename = $user[0] . '-product-headline';

        $config['upload_path'] = './uploads/product';
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 10240;

        $this->load->library('upload', $config);
        $this->upload->do_upload('product_path');

        $editProduct->path = $filename . $this->upload->data('file_ext');
        if (empty($_FILES['product_path']['name'])) {
            $editProduct->path = "";
        }

        $result = $this->product->edit($productId, $editProduct);

        if ($result > 0) {
            // success
            $this->session->set_flashdata('edit', true);
            redirect('product/' . $productId);
        } else {
            $this->session->set_flashdata('edit', false);
            redirect('product/' . $productId);
        }
    }

    public function addGallery()
    {
        $productId = $this->input->post('id');
        $fileCount = count($_FILES['photo_paths']['name']);

        $user = explode('@', $this->session->userdata('email'));

        $config['upload_path'] = './uploads/product';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 10240;

        $this->load->library('upload');

        $count = 0;
        for ($i = 0; $i < $fileCount; $i++) {
            $_FILES['photo_path']['name'] = $_FILES['photo_paths']['name'][$i];
            $_FILES['photo_path']['type'] = $_FILES['photo_paths']['type'][$i];
            $_FILES['photo_path']['tmp_name'] = $_FILES['photo_paths']['tmp_name'][$i];
            $_FILES['photo_path']['error'] = $_FILES['photo_paths']['error'][$i];
            $_FILES['photo_path']['size'] = $_FILES['photo_paths']['size'][$i];
            ++$count;
            $config['file_name'] = str_replace(".", "", $user[0]).'-product-'.$productId.'-gallery-'.($count);
            $config['overwrite'] = true;
            $this->upload->initialize($config);

            $this->upload->do_upload('photo_path');
            $photo =  str_replace(".", "", $user[0]).'-product-'.$productId.'-gallery-'.($count).$this->upload->data('file_ext');
            $this->product->addGallery($productId, $photo);
        }
        redirect('product/' . $productId);
    }

    public function updateGallery()
    {
      $file = explode('.', $this->input->post('existing_path'));
      $filename = $file[0];

      $config['upload_path'] = './uploads/product';
      $config['file_name'] = str_replace('.', "", $filename);
      $config['overwrite'] = true;
      $config['allowed_types'] = 'jpg|png';
      $config['max_size'] = 10240;

      $this->load->library('upload', $config);
      $this->upload->do_upload('photo');
    }

    public function addProduct()
    {
        $userProfile = $this->user->getProfile($this->session->userdata('email'));

        $storeId = $userProfile->row()->store_id;
        $product = $this->input->post('product');
        $desc = $this->input->post('description');
        $amount = $this->input->post('amount');
        $price = $this->input->post('price');

        $newProduct = new Product();
        $newProduct->name = $product;
        $newProduct->desc = $desc;
        $newProduct->amount = $amount;
        $newProduct->price = $price;
        $newProduct->storeId = $storeId;

        $user = explode('@', $userProfile->row()->email);
        $filename = $user[0] . '-product-headline';

        $config['upload_path'] = './uploads/product';
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 10240;

        $this->load->library('upload', $config);
        $this->upload->do_upload('product_path');

        $newProduct->path = $filename . $this->upload->data('file_ext');
        if (empty($_FILES['product_path']['name'])) {
            $path = "";
        }

        $result = $this->product->create($newProduct);

        if ($result > 0) {
            // success
            $this->session->set_flashdata('product', true);
            redirect('my-store');
        } else {
            $this->session->set_flashdata('product', false);
            redirect('my-store');
        }
    }
}
