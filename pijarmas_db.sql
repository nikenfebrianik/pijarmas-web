-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2019 at 04:40 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pijarmas_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `amount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `product_path` varchar(191) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `name`, `description`, `amount`, `price`, `product_path`, `store_id`) VALUES
(1, 'Barakuda Crispi', 'Keunggulan\r\n1. Renyah\r\n2. Gurih\r\n3. Bergizi\r\n4. Tahan Lama\r\n5. Dari laut langsung di olah', 1, 8000, '11.jpeg', 2),
(2, 'Abon Ikan Gabus', 'Keunggulan\r\n1. Gurih\r\n2. Mengandung Protein Penyembuh penyakit luka\r\n', 1, 25000, 'abon-ikan-gabus.jpg', 2),
(3, 'Abon Pedo', 'Keunggulan\r\n1. Gurih Lezat dan Bergizi', 1, 25000, 'abon-pedo.jpg', 2),
(4, 'Egg Roll Gumila', 'Keunggulan\r\n1.Bergizi  terbuat dari SORGUM dan ikan Nila \r\n2. Cocok Buat Cemilan\r\n', 1, 15000, 'egg-roll-gumila.jpg', 4),
(5, 'Egg Roll Janila', 'Keunggulan\r\n1.Bergizi  terbuat dari SORGUM dan ikan Nila\r\n2. Cocok Buat Cemilan', 1, 15000, 'egg-roll-janila.jpg', 5),
(6, 'Petis Udang', 'Keunggulan\r\n1. Bergizi Gurih Nikmat Lezat\r\n2. Langsung Di cocol dgn gorengan\r\n', 1, 15000, 'petis-udang.jpg', 6),
(7, 'Petis Udang Rasa Pedas', 'Keunggulan\r\n1. Bergizi Gurih Nikmat Lezat\r\n2. Langsung Di cocol dgn gorengan\r\n3. Tidak usah Pakai cabai \r\n', 1, 15000, 'petis-udang-rasa-pedas.jpg', 6),
(8, 'Petis Bandeng Presto', 'Keunggulan\r\n1. Bergizi Gurih Nikmat Lezat\r\n2. Langsung Di cocol dgn gorengan', 1, 15000, 'petis-bandeng-presto.jpg', 7),
(9, 'Krupuk Catak', 'Keunggulan\r\n1. Mengadung banyak Protein\r\n2. Terbuat dari Lambung Ikan Manyung', 1, 30000, 'krupuk-catak.jpg', 8),
(10, 'Kripik Gimbal Udang', 'Keunggulan\r\n1. Mengadung banyak Protein\r\n2. Terbuat dari Udang segar laut\r\n', 1, 20000, 'krupuk-gimbal-udang.jpg', 8),
(11, 'Dendeng Ikan Gurih', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Tawar Gurih', 1, 25000, 'dendeng-ikan-gurih.jpg', 9),
(12, 'Dendeng Ikan Manis', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Manis', 1, 25000, 'dendeng-ikan-manis.jpg', 9),
(13, 'Dendeng Ikan Gurih', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Tawar Gurih', 1, 25000, 'rizky-dendeng-ikan-gurih.jpg', 10),
(14, 'Dendeng Ikan Gurih', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Tawar Gurih', 1, 25000, 'wati-dendeng-ikan-gurih.jpg', 11),
(15, 'Ikan Layur Cripsy', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Tawar Gurih', 1, 15000, '4.jpeg', 12),
(16, 'Stik Ikan', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat Cemilan\r\n3. Rasa Ikan', 1, 15000, '7.jpeg', 13),
(17, 'Abon Ikan Bandeng', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Ikan Bandeng Gurih', 1, 25000, '16.jpeg', 14),
(18, 'Balado Ikan Teri Asin', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat lauk\r\n3. Rasa Ikan Gurih', 1, 15000, '15.jpeg', 12),
(19, 'Bola Ikan Tenggiri', 'Keunggulan\r\n1. GURIH Bergizi\r\n2. Renyah cocok Buat Cemilan\r\n3. Rasa Ikan', 1, 18000, '9.jpeg', 13),
(20, 'Wader Laut Crispy', 'Keunggulan\r\n1. Renyah\r\n2. Gurih\r\n3. Bergizi\r\n4. Tahan Lama\r\n5. Dari laut langsung di olah\r\n', 1, 15000, '3.jpeg', 12);

-- --------------------------------------------------------

--
-- Table structure for table `product_galleries`
--

CREATE TABLE `product_galleries` (
  `id` int(11) NOT NULL,
  `photo_path` varchar(191) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` varchar(3) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
('adm', 'admin'),
('sel', 'seller'),
('usr', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `store_id` int(11) NOT NULL,
  `name` varchar(140) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `store_path` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`store_id`, `name`, `address`, `phone`, `store_path`) VALUES
(1, 'Toko Sehat', 'PERUM WIKU II Jalan Flamboyan 3 Blok V3 Katonsari', '8112326009', 'nikenfebrianik-store-photo.png'),
(2, 'Haruan', 'Karang Tengah', '83824702925', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(3, 'Barakuda', 'Wedung', '82241376509', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(4, 'Restu Wali', 'Wonosalam', '823023435854', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(5, 'Mekar Jaya', 'Guntur', '85292313431', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(6, 'Nur Jaya', 'Gaji - Guntur', '85290758113', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(7, 'Setia Kawan', 'Wedung', '85290758113', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(8, 'Rejeki Slamet', 'Wedung', '', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(9, 'Asri', 'Wedung', '87875983515', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(10, 'Rizki Lancar', 'Wedung', '87875983515', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(11, 'Mba Wati', 'Wedung', '87875983515', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(12, 'Hana Snack', 'Wedung', '87875983515', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(13, 'Kaila Snack', 'Karang Tengah', '87810884601', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png'),
(14, 'Rosyada', 'Sayung', '83836561805', 'https://www.freeiconspng.com/uploads/retail-store-icon-15.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `profile_path` varchar(191) DEFAULT NULL,
  `role_id` varchar(3) NOT NULL,
  `store_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fullname`, `email`, `password`, `profile_path`, `role_id`, `store_id`) VALUES
(4, 'Niken', 'nikenfebrianik@gmail.com', '123', 'nikenfebrianik-profile-photo.png', 'sel', 1),
(5, 'Barakuda', 'barakuda@mail.com', 'pijarmas', NULL, 'sel', 3),
(6, 'Haruan', 'haruan@mail.com', 'pijarmas', NULL, 'sel', 2),
(7, 'Restu Wali', 'restuwali@mail.com', 'pijarmas', NULL, 'sel', 4),
(8, 'Mekar Jaya', 'mekarjaya@mail.com', 'pijarmas', NULL, 'sel', 5),
(9, 'Nur Jaya', 'nurjaya@mail.com', 'pijarmas', NULL, 'sel', 6),
(10, 'Setia Kawan', 'setiakawan@mail.com', 'pijarmas', NULL, 'sel', 7),
(11, 'Rejeki Slamet', 'rejekislamet@mail.comm', 'pijarmas', NULL, 'sel', 8),
(12, 'Asri', 'asri@mail.com', 'pijarmas', NULL, 'sel', 9),
(13, 'Rizki Lancar', 'rizkilancar@mail.com', 'pijarmas', NULL, 'sel', 10),
(14, 'Wati', 'wati@mail.com', 'pijarmas', NULL, 'sel', 11),
(15, 'Hana Snack', 'hanasnack@mail.com', 'pijarmas', NULL, 'sel', 12),
(16, 'Kaila Snack', 'kailasnack@mail.com', 'pijarmas', NULL, 'sel', 13),
(17, 'Rosyada', 'rosyada@mail.com', 'pijarmas', NULL, 'sel', 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `fkIdx_73` (`store_id`);

--
-- Indexes for table `product_galleries`
--
ALTER TABLE `product_galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_55` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `fkIdx_22` (`role_id`),
  ADD KEY `fkIdx_80` (`store_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_galleries`
--
ALTER TABLE `product_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_73` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`);

--
-- Constraints for table `product_galleries`
--
ALTER TABLE `product_galleries`
  ADD CONSTRAINT `FK_55` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_22` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `FK_80` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
